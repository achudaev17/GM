﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace Garment_Manufacturing
{
    class Authorizator
    {
        //Запрос к бд
        private string getUsersData = "" +
            "select Пользователи.Логин, Пользователи.Пароль, Роли.Наименование_Роли " +
            "from Пользователи inner join Роли on Пользователи.Роль = Роли.РольID";
        //Флаг корректности введенных данных
        private bool isCorrect = false;
        public string userRole;

        /// <summary>
        /// Метод, запускающий авторизацию и возвращающий роль пользователя, если авторизация успешна
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string GetUserRole(string login, string password)
        {
            try
            {
                //Выполнение запроса на авторизацию
                Connector.SearchValuesQuery(getUsersData);
                //Перебор логина и пароля циклом
                for (int i = 0; i < Connector.ds.Tables[0].Rows.Count; i++)
                {
                    //Проверка логина и пароля на совпадение с введенными данными
                    if (login == Connector.ds.Tables[0].Rows[i][0].ToString() && password == Connector.ds.Tables[0].Rows[i][1].ToString())
                    {
                        //Если все правильно, то возвращается роль пользователя, для открытия формы соответствующего уровня доступа
                        userRole = Connector.ds.Tables[0].Rows[i][2].ToString();
                        //Установка авторизованного пользователя
                        Connector.SetAuthorizedUser(Connector.ds.Tables[0].Rows[i][0].ToString());
                        isCorrect = true;
                        return userRole;
                    }
                    else
                    {
                        isCorrect = false;
                    }
                }

                if (!isCorrect)
                {
                    throw new Exception("Неправильный логин или пароль");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return userRole;
        }

    }
}
