﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Garment_Manufacturing
{
    public partial class Manager : Form
    {
        private Form AuthForm;
        private string query = "select Изделия.Наименование_Изделия, Заказы.Количество, Заказы.Сумма_Заказа, Пользователи.Фамилия, Пользователи.Имя " +
            "from Заказы_Пользователей inner join Заказы on Заказы_Пользователей.Заказ = Заказы.ID_Заказа inner join Изделия on Заказы.Товар = Изделия.ИзделиеID " +
            "inner join Пользователи on Заказы_Пользователей.Пользователь = Пользователи.ID_Пользователя";
        public Manager(Authorization auth)
        {
            InitializeComponent();
            AuthForm = auth; 
            UsersOrdersData.DataSource = Connector.SearchValuesQuery(query);
        }

        private void Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            AuthForm.Show();
            Hide();
        }
    }
}
