﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Garment_Manufacturing
{
    public partial class FurnitureCounting : Form
    {
        //Запрос для получения данных о фурнитуре
        private string FurnitureQuery =  @"" +
                    "select ФурнитураID as Артикул, Наименование_Фурнитуры as Наименование, Тип_Фурнитуры as Тип," +
                    "Ширина_Фурнитуры as Ширина, Длина_Фурнитуры as Длина, Вес_Фурнитуры as Вес, Цена_Фурнитуры as Цена from Фурнитуры";
        private Form CourierForm;
        public FurnitureCounting(Courier courier)
        {
            CourierForm = courier;
            InitializeComponent();
            FurnitureData.DataSource = Connector.SearchValuesQuery(FurnitureQuery);
        }

        private void GetBackButton_Click(object sender, EventArgs e)
        {
            Hide();
            CourierForm.Show();
        }

        private void FurnitureCounting_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
