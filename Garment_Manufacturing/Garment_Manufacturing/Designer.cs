﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Garment_Manufacturing
{
    public partial class Designer : Form
    {
        private Form AuthForm;
        public Designer(Authorization auth)
        {
            InitializeComponent();
            AuthForm = auth;
        }

        private void Designer_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Hide();
            AuthForm.Show();
        }
    }
}
