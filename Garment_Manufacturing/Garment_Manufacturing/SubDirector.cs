﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Garment_Manufacturing
{
    public partial class SubDirector : Form
    {
        private Form AuthForm;
        private string getUsersData = "select * from Пользователи where Роль = 7";
        private int userRole;
        
        public SubDirector(Authorization Auth)
        {
            AuthForm = Auth;
            InitializeComponent();
            dataGridView1.DataSource = Connector.SearchValuesQuery(getUsersData);

        }

        /// <summary>
        /// Метод, устанавливающий роль пользователю, у которого она отсутствует
        /// </summary>
        /// <param name="employeeID"></param>
        private void GiveRoleToUser(int employeeID)
        {
            SwitchRole();
            string setRoleQuery = @"update Пользователи set Роль =" + userRole + " where ID_Пользователя =" + employeeID;
            Connector.Execute(setRoleQuery);
            MessageBox.Show("Роль пользователя с ID " + employeeID + " успешно изменена");
        }

        /// <summary>
        /// Метод проверяющий роль пользователя
        /// </summary>
        /// <returns>Выбранную роль пользователя</returns>
        private int SwitchRole()
        {
            switch (RoleCollection.SelectedItem)
            {
                case "Охрана":
                    userRole = 1;
                    break;
                case "Менеджер":
                    userRole = 2;
                    break;
                case "Зам.Директора":
                    userRole = 3;
                    break;
                case "Дизайнер":
                    userRole = 4;
                    break;
                case "Курьер":
                    userRole = 5;
                    break;
            }

            return userRole;
        }

        private void SubDirector_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void GiveRole_Click(object sender, EventArgs e)
        {
            GiveRoleToUser(Convert.ToInt32(textBox1.Text));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AuthForm.Show();
            Hide();
        }
    }
}
