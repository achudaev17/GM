﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Garment_Manufacturing
{
    public partial class Authorization : Form
    {
        Authorizator ath = new Authorizator();
        
        public Authorization()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Метод, показывающий скрытый пароль
        /// </summary>
        private void ShowPassword()
        {
            if (PasswordShow.Checked)
            {
                PasswordInput.UseSystemPasswordChar = false;
            }
            else
            {
                PasswordInput.UseSystemPasswordChar = true;
            }
        }

        /// <summary>
        /// Метод, проверяющий роль пользователя и открывающий форму соответствующую уровню доступа
        /// </summary>
        /// <param name="ath">Экзекмпляр класcа Authorization</param>
        private void StartAuth(Authorizator ath)
        {

            ath.GetUserRole(LoginInput.Text, PasswordInput.Text);
            try
            {
                switch (ath.userRole)
                {
                    case "Охрана":
                        Security security = new Security(this);
                        security.Show();
                        Hide();
                        break;
                    case "Менеджер":
                        Manager manager = new Manager(this);
                        manager.Show();
                        Hide();
                        break;
                    case "Дизайнер":
                        Designer designer = new Designer(this);
                        designer.Show();
                        Hide();
                        break;
                    case "Курьер":
                        Courier courier = new Courier(this);
                        courier.Show();
                        Hide();
                        break;
                    case "Зам.Директора":
                        SubDirector subdirector = new SubDirector(this);
                        subdirector.Show();
                        Hide();
                        break;
                    case "Пользователь":
                        User user = new User(this);
                        user.Show();
                        Hide();
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AuthButton_Click(object sender, EventArgs e)
        {
            StartAuth(ath);
            LoginInput.Text = "";
            PasswordInput.Text = "";
        }

        private void PasswordShow_CheckedChanged(object sender, EventArgs e)
        {
            ShowPassword();
        }

        private void Auth_Load(object sender, EventArgs e)
        {
            ShowPassword();
        }

        //Главная форма передается в конструктор дочерней формы экземпляр класса родительской
        //Чтобы потом можно было в дочерней вернуться на родительскую
        private void RegisterLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //Создается экземпляр класса дочерней формы
            Registration reg = new Registration(this);
            reg.Show();
            Hide();
        }

    }
}
