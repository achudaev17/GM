﻿namespace Garment_Manufacturing
{
    partial class ClothCounting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ClothData = new System.Windows.Forms.DataGridView();
            this.GetBackButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ClothData)).BeginInit();
            this.SuspendLayout();
            // 
            // ClothData
            // 
            this.ClothData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ClothData.Location = new System.Drawing.Point(12, 12);
            this.ClothData.Name = "ClothData";
            this.ClothData.Size = new System.Drawing.Size(776, 317);
            this.ClothData.TabIndex = 0;
            // 
            // GetBackButton
            // 
            this.GetBackButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(252)))), ((int)(((byte)(214)))));
            this.GetBackButton.Location = new System.Drawing.Point(713, 354);
            this.GetBackButton.Name = "GetBackButton";
            this.GetBackButton.Size = new System.Drawing.Size(75, 23);
            this.GetBackButton.TabIndex = 1;
            this.GetBackButton.Text = "Назад";
            this.GetBackButton.UseVisualStyleBackColor = false;
            this.GetBackButton.Click += new System.EventHandler(this.GetBackButton_Click);
            // 
            // ClothCounting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(238)))), ((int)(((byte)(252)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GetBackButton);
            this.Controls.Add(this.ClothData);
            this.Name = "ClothCounting";
            this.Text = "Учет тканей";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClothCounting_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ClothData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ClothData;
        private System.Windows.Forms.Button GetBackButton;
    }
}