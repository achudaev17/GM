﻿namespace Garment_Manufacturing
{
    partial class User
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Header = new System.Windows.Forms.Label();
            this.ExitButton = new System.Windows.Forms.Button();
            this.ProductsData = new System.Windows.Forms.DataGridView();
            this.ProductName = new System.Windows.Forms.Label();
            this.CountLabel = new System.Windows.Forms.Label();
            this.CountInput = new System.Windows.Forms.TextBox();
            this.OrderButton = new System.Windows.Forms.Button();
            this.ProdsCollection = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.ProductsData)).BeginInit();
            this.SuspendLayout();
            // 
            // Header
            // 
            this.Header.AutoSize = true;
            this.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Header.Location = new System.Drawing.Point(25, 23);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(143, 18);
            this.Header.TabIndex = 0;
            this.Header.Text = "Товары для заказа";
            // 
            // ExitButton
            // 
            this.ExitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(252)))), ((int)(((byte)(214)))));
            this.ExitButton.Location = new System.Drawing.Point(491, 434);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(75, 23);
            this.ExitButton.TabIndex = 1;
            this.ExitButton.Text = "Выйти";
            this.ExitButton.UseVisualStyleBackColor = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // ProductsData
            // 
            this.ProductsData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductsData.Location = new System.Drawing.Point(28, 54);
            this.ProductsData.Name = "ProductsData";
            this.ProductsData.Size = new System.Drawing.Size(538, 295);
            this.ProductsData.TabIndex = 2;
            // 
            // ProductName
            // 
            this.ProductName.AutoSize = true;
            this.ProductName.Location = new System.Drawing.Point(25, 364);
            this.ProductName.Name = "ProductName";
            this.ProductName.Size = new System.Drawing.Size(102, 13);
            this.ProductName.TabIndex = 3;
            this.ProductName.Text = "Выберите изделие";
            // 
            // CountLabel
            // 
            this.CountLabel.AutoSize = true;
            this.CountLabel.Location = new System.Drawing.Point(362, 367);
            this.CountLabel.Name = "CountLabel";
            this.CountLabel.Size = new System.Drawing.Size(66, 13);
            this.CountLabel.TabIndex = 5;
            this.CountLabel.Text = "Количество";
            // 
            // CountInput
            // 
            this.CountInput.Location = new System.Drawing.Point(365, 393);
            this.CountInput.Name = "CountInput";
            this.CountInput.Size = new System.Drawing.Size(100, 20);
            this.CountInput.TabIndex = 6;
            // 
            // OrderButton
            // 
            this.OrderButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(252)))), ((int)(((byte)(214)))));
            this.OrderButton.Location = new System.Drawing.Point(491, 392);
            this.OrderButton.Name = "OrderButton";
            this.OrderButton.Size = new System.Drawing.Size(75, 23);
            this.OrderButton.TabIndex = 7;
            this.OrderButton.Text = "Заказать";
            this.OrderButton.UseVisualStyleBackColor = false;
            this.OrderButton.Click += new System.EventHandler(this.OrderButton_Click);
            // 
            // ProdsCollection
            // 
            this.ProdsCollection.FormattingEnabled = true;
            this.ProdsCollection.Location = new System.Drawing.Point(28, 392);
            this.ProdsCollection.Name = "ProdsCollection";
            this.ProdsCollection.Size = new System.Drawing.Size(321, 21);
            this.ProdsCollection.TabIndex = 8;
            // 
            // User
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(238)))), ((int)(((byte)(252)))));
            this.ClientSize = new System.Drawing.Size(599, 479);
            this.Controls.Add(this.ProdsCollection);
            this.Controls.Add(this.OrderButton);
            this.Controls.Add(this.CountInput);
            this.Controls.Add(this.CountLabel);
            this.Controls.Add(this.ProductName);
            this.Controls.Add(this.ProductsData);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.Header);
            this.Name = "User";
            this.Text = "Пользователь";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.User_FormClosing);
            this.Load += new System.EventHandler(this.User_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ProductsData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Header;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.DataGridView ProductsData;
        private System.Windows.Forms.Label ProductName;
        private System.Windows.Forms.Label CountLabel;
        private System.Windows.Forms.TextBox CountInput;
        private System.Windows.Forms.Button OrderButton;
        private System.Windows.Forms.ComboBox ProdsCollection;
    }
}