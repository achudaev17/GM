﻿namespace Garment_Manufacturing
{
    partial class FurnitureCounting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FurnitureData = new System.Windows.Forms.DataGridView();
            this.GetBackButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.FurnitureData)).BeginInit();
            this.SuspendLayout();
            // 
            // FurnitureData
            // 
            this.FurnitureData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FurnitureData.Location = new System.Drawing.Point(26, 29);
            this.FurnitureData.Name = "FurnitureData";
            this.FurnitureData.Size = new System.Drawing.Size(732, 330);
            this.FurnitureData.TabIndex = 0;
            // 
            // GetBackButton
            // 
            this.GetBackButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(252)))), ((int)(((byte)(214)))));
            this.GetBackButton.Location = new System.Drawing.Point(673, 399);
            this.GetBackButton.Name = "GetBackButton";
            this.GetBackButton.Size = new System.Drawing.Size(75, 23);
            this.GetBackButton.TabIndex = 1;
            this.GetBackButton.Text = "Назад";
            this.GetBackButton.UseVisualStyleBackColor = false;
            this.GetBackButton.Click += new System.EventHandler(this.GetBackButton_Click);
            // 
            // FurnitureCounting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(238)))), ((int)(((byte)(252)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GetBackButton);
            this.Controls.Add(this.FurnitureData);
            this.Name = "FurnitureCounting";
            this.Text = "Учет Фурнитуры";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FurnitureCounting_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.FurnitureData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView FurnitureData;
        private System.Windows.Forms.Button GetBackButton;
    }
}