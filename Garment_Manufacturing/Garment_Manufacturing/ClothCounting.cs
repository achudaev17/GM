﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Garment_Manufacturing
{
    public partial class ClothCounting : Form
    {
        private Form CourierForm;
        private string query = @"select ТканьID as Артикул, Наименование_Ткани as Наименование, Цвет_Ткани as Цвет, Рисунок_Ткани as Рисунок, Состав_ткани as Состав," +
            "Длина_Ткани as Длина, Ширина_Ткани as Ширина, Цена_Ткани as Цена from Ткани";
        public ClothCounting(Courier courier)
        {
            InitializeComponent();
            CourierForm = courier;
            ClothData.DataSource = Connector.SearchValuesQuery(query);
        }

        private void GetBackButton_Click(object sender, EventArgs e)
        {
            Hide();
            CourierForm.Show();
        }

        private void ClothCounting_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
