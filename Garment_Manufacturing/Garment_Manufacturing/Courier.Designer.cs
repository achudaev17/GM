﻿namespace Garment_Manufacturing
{
    partial class Courier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeaderLabel = new System.Windows.Forms.Label();
            this.FurnitureButton = new System.Windows.Forms.Button();
            this.ClothButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.AutoSize = true;
            this.HeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HeaderLabel.Location = new System.Drawing.Point(112, 32);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(41, 18);
            this.HeaderLabel.TabIndex = 0;
            this.HeaderLabel.Text = "Учет";
            // 
            // FurnitureButton
            // 
            this.FurnitureButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(252)))), ((int)(((byte)(214)))));
            this.FurnitureButton.Location = new System.Drawing.Point(95, 78);
            this.FurnitureButton.Name = "FurnitureButton";
            this.FurnitureButton.Size = new System.Drawing.Size(75, 23);
            this.FurnitureButton.TabIndex = 1;
            this.FurnitureButton.Text = "Фурнитуры";
            this.FurnitureButton.UseVisualStyleBackColor = false;
            this.FurnitureButton.Click += new System.EventHandler(this.FurnitureButton_Click);
            // 
            // ClothButton
            // 
            this.ClothButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(252)))), ((int)(((byte)(214)))));
            this.ClothButton.Location = new System.Drawing.Point(95, 116);
            this.ClothButton.Name = "ClothButton";
            this.ClothButton.Size = new System.Drawing.Size(75, 23);
            this.ClothButton.TabIndex = 2;
            this.ClothButton.Text = "Тканей";
            this.ClothButton.UseVisualStyleBackColor = false;
            this.ClothButton.Click += new System.EventHandler(this.ClothButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(252)))), ((int)(((byte)(214)))));
            this.ExitButton.Location = new System.Drawing.Point(95, 150);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(75, 23);
            this.ExitButton.TabIndex = 3;
            this.ExitButton.Text = "Выйти";
            this.ExitButton.UseVisualStyleBackColor = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // Courier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(238)))), ((int)(((byte)(252)))));
            this.ClientSize = new System.Drawing.Size(267, 232);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.ClothButton);
            this.Controls.Add(this.FurnitureButton);
            this.Controls.Add(this.HeaderLabel);
            this.Name = "Courier";
            this.Text = "Кладовщик";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Courier_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeaderLabel;
        private System.Windows.Forms.Button FurnitureButton;
        private System.Windows.Forms.Button ClothButton;
        private System.Windows.Forms.Button ExitButton;
    }
}