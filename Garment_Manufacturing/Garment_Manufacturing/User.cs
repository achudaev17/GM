﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Garment_Manufacturing
{
    public partial class User : Form
    {
        private Form AuthForm;
        private string GetProductsData = "select ИзделиеID as Артикул, Наименование_Изделия as Наименование, Ширина_Изделия as Ширина, Длина_Изделия as Длина, Стоимость from Изделия";
        private string GetProductsNames = "select Наименование_Изделия from Изделия";
        private string ProductID;
        private string AuthUserName;
        public User(Authorization auth)
        {
            InitializeComponent();
            AuthForm = auth;
            ProductsData.DataSource = Connector.SearchValuesQuery(GetProductsData);

        }

        private void User_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            AuthForm.Show();
            Hide();
        }

        /// <summary>
        /// Метод получения ID выбранного продукта
        /// </summary>
        /// <param name="prodName">Название выбранного продукта</param>
        /// <returns></returns>
        private string GetProductID(string prodName)
        {
            Connector.SearchValuesQuery("select ИзделиеID from Изделия where Наименование_Изделия = " + "\'" + prodName + "\'");
            ProductID = Connector.ds.Tables[0].Rows[0][0].ToString();
            return ProductID;
        }

        /// <summary>
        /// Метод получения ID авторизованного пользователя
        /// </summary>
        /// <param name="AuthUser">Логин авторизованного пользователя</param>
        /// <returns></returns>
        private string GetAuthorizedUser(string AuthUser)
        {
            Connector.SearchValuesQuery("select ID_Пользователя from Пользователи where Логин = " + "\'" + Connector.AuthorizedUser + "\'");
            AuthUserName = Connector.ds.Tables[0].Rows[0][0].ToString();
            return AuthUserName;
        }

        private void OrderButton_Click(object sender, EventArgs e)
        {
            GetProductID(ProdsCollection.SelectedItem.ToString());
            Connector.Execute("" +
                "insert into Заказы(Товар, Количество) values(" +
                "\'" + ProductID + "\'" + "," + "\'" + Convert.ToInt32(CountInput.Text) + "\'" + ")");
            Connector.SearchValuesQuery("select max(ID_Заказа) from Заказы");
            int orderID = Convert.ToInt32(Connector.ds.Tables[0].Rows[0][0].ToString());
            GetAuthorizedUser(Connector.AuthorizedUser);
            Connector.Execute("" +
                "insert into Заказы_Пользователей(Заказ, Пользователь) values (" +
                "\'" + orderID + "\'" + "," + "\'" + AuthUserName + "\'" + ")");
            MessageBox.Show("Заказ успешно оформлен");
        }

        private void User_Load(object sender, EventArgs e)
        {
            Connector.SearchValuesQuery(GetProductsNames);
            for (int i = 0; i < Connector.ds.Tables[0].Rows.Count; i++)
            {
                ProdsCollection.Items.Add(Connector.ds.Tables[0].Rows[i][0].ToString());
            }
        }
    }
}
