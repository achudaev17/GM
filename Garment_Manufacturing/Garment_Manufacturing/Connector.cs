﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Garment_Manufacturing
{
    /// <summary>
    /// Класс для работы с запросами к базе данных
    /// </summary>
    class Connector
    {
        //Защищенная строка подключения к базе данных
        private static string conn = @"Data Source=localhost; Initial Catalog = Производство_Швейное; Integrated Security=true";

        //Объекты необходимые для работы с бд
        public static DataSet ds;
        public static string AuthorizedUser;
        private static SqlDataAdapter sqlad;
        private static SqlCommand comnd;

        /// <summary>
        /// Модифицированный метод выполнения запроса, возвращающий результат выполнения запроса
        /// </summary>
        /// <param name="QueryString">Строка запроса</param>
        /// <returns>Таблицу, как результат выполнения запроса</returns>
        public static object SearchValuesQuery(string QueryString)
        {
            using (SqlConnection sqlconn = new SqlConnection(conn))
            {
                Execute(QueryString);
                sqlad.Fill(ds);
                return ds.Tables[0];
            }
        }

        /// <summary>
        /// Метод устанвливающий какой именно пользователь авторизовался
        /// </summary>
        /// <param name="UserLogin">Логин пользователя</param>
        public static void SetAuthorizedUser(string UserLogin)
        {
            AuthorizedUser = UserLogin;
        }

        /// <summary>
        /// Метод выполняющий запрос
        /// </summary>
        /// <param name="QueryString"></param>
        public static void Execute(string QueryString)
        {
            using (SqlConnection sqlconn = new SqlConnection(conn))
            {
                sqlconn.Open();
                sqlad = new SqlDataAdapter(QueryString, conn);
                ds = new DataSet();
                comnd = new SqlCommand(QueryString, sqlconn);
                comnd.ExecuteNonQuery();
                sqlconn.Close();
            }
        }
    }
}
