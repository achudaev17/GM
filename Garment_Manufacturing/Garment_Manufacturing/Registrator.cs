﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Garment_Manufacturing
{
    class Registrator
    {
        private string query;
        /// <summary>
        /// Метод, регистрирующий пользователя
        /// </summary>
        /// <param name="surname">Фамилия пользователя</param>
        /// <param name="name">Имя пользователя</param>
        /// <param name="lastName">Отчество пользователя</param>
        /// <param name="role">Роль, выбираемая из Combobox'a</param>
        /// <param name="login">Логин</param>
        /// <param name="password">Пароль</param>
        /// <param name="passConfirm">Подтверждение пароля</param>
        public string Register(
            string surname, 
            string name, 
            string lastName, 
            string login, 
            string password, 
            string passConfirm)
        {
            query = @"insert into Пользователи(Роль, Логин, Пароль, Фамилия, Имя, Отчество)" +
                "values(" + "\'" + 7 + "\'" + "," + "\'" + login + "\'" + "," + "\'" + password + "\'" + "," + "\'" + surname + "\'" + "," + "\'" + name + "\'" + "," + "\'" + lastName + "\'" + ")";

            try
            {
                //Проверка заполнения полей
                if (surname != "" && name != "" && lastName != ""  && login != "" && password != "" && passConfirm != "")
                {
                    //Проверка соотвествия пароля
                    if (password == passConfirm)
                    {
                        //Если правильно, то выполняется запрос
                        Connector.Execute(query);
                        MessageBox.Show("Вы успешно зарегистрировались");
                        return "Success";
                    }
                    else
                    {
                        //Иначе выбрасывается ошибка
                        throw new Exception("Пароли не совпадают");
                    }
                }
                else
                {
                    //Выброс ошибки
                    throw new Exception("Все поля обязательны для заполнения!" + Environment.NewLine + "Проверьте правильность ввода");
                } 
            } 
            catch (Exception ex)
            {
                //Обработка ошибок
                MessageBox.Show(ex.Message);
            }

            return "Fail";

        }


    }
}
