﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Garment_Manufacturing
{
    public partial class Courier : Form
    {
        private Form AuthForm;
        public Courier(Authorization auth)
        {
            InitializeComponent();
            AuthForm = auth;
        }

        private void Courier_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void FurnitureButton_Click(object sender, EventArgs e)
        {
            Hide();
            FurnitureCounting fc = new FurnitureCounting(this);
            fc.Show();
        }

        private void ClothButton_Click(object sender, EventArgs e)
        {
            Hide();
            ClothCounting cc = new ClothCounting(this);
            cc.Show();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            AuthForm.Show();
            Hide();
        }
    }
}
