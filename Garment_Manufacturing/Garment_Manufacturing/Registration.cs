﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Garment_Manufacturing
{
    public partial class Registration : Form
    {
        private Form AuthFormOpen;

        /// <summary>
        /// Конструктор класса Registration
        /// </summary>
        /// <param name="auth">Экземпляр формы авторизации</param>
        public Registration(Authorization auth)
        {
            InitializeComponent();
            AuthFormOpen = auth;
        }

        /// <summary>
        /// Метод, возвращающий прерыдущую форму
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetBack_Click(object sender, EventArgs e)
        {
            Hide();
            AuthFormOpen.Show();
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            try
            {
                Registrator register = new Registrator();

                //Регистрация пользователя и проверка на то что она прошла успешно
                //Если успешно, то все поля ввода очищаются
                if (register.Register(
                    SurnameInput.Text,
                    NameInput.Text,
                    LastNameInput.Text,
                    LoginInput.Text,
                    PasswordInput.Text,
                    PassConfirmInput.Text
                    ) == "Success")
                {
                    SurnameInput.Text = "";
                    NameInput.Text = "";
                    LastNameInput.Text = "";
                    LoginInput.Text = "";
                    PasswordInput.Text = "";
                    PassConfirmInput.Text = "";
                } 

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Registration_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
